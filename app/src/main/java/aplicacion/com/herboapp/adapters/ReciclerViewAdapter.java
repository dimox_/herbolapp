package aplicacion.com.herboapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import aplicacion.com.herboapp.R;
import aplicacion.com.herboapp.activities.DetalleProductoActivity;
import aplicacion.com.herboapp.models.Producto;

/**
 * Clase que implementa un adaptador para ser utilizado en un ReciclerView
 */
public class ReciclerViewAdapter extends RecyclerView
        .Adapter<ReciclerViewAdapter.ProductosViewHolder>{

    private ArrayList<Producto> productosList;

    public ReciclerViewAdapter(ArrayList<Producto> productosList) {
        this.productosList = productosList;
    }

    /**
     * Se llama cuando RecyclerView necesita un nuevo RecyclerView.ViewHolder del tipo dado
     * para representar un elemento.
     * @param parent El ViewGroup en el que se agregara la nueva vista despues de que este
     * enlazado a una posicion de adaptador.
     * @param viewType El tipo de vista de la nueva vista.
     * @return Objeto ProductosViewHolder
     */
    @Override
    public ProductosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_listproductos_monitor, parent,false);

        ProductosViewHolder holder = new ProductosViewHolder(view);


        return holder;
    }

    /**
     * Llamado por RecyclerView para mostrar los datos en la posición especificada.
     * @param holder El ViewHolder que debe actualizarse para representar el contenido
     * del elemento en la posicion dada en el conjunto de datos.
     * @param position La posición del elemento dentro del conjunto de datos del adaptador.
     */
    @Override
    public void onBindViewHolder(ProductosViewHolder holder, int position) {

        final Producto producto = productosList.get(position);

        holder.txtNombre.setText(producto.getNombre());
        holder.txtCodigo.setText(" Cod: " + producto.getCodigo());
        holder.txtExistencias.setText(String.valueOf(producto.getExistencia()));

        final Context context = holder.txtExistencias.getContext();

        if ( producto.getExistencia() <= 0 ){
            holder.txtExistencias.setTextColor(ContextCompat.getColor(context, R.color.colorDanger));
            holder.imgEstado.setImageResource(R.drawable.estado1);
        }
        else if ( producto.getExistencia() <= 50 &&  producto.getExistencia() > 0 ){
            holder.txtExistencias.setTextColor(ContextCompat.getColor(context, R.color.colorDanger));
            holder.imgEstado.setImageResource(R.drawable.estado2);
        }
        else if (producto.getExistencia() <= 100 && producto.getExistencia() > 50 ){
            holder.txtExistencias.setTextColor(ContextCompat.getColor(context, R.color.colorWarning));
            holder.imgEstado.setImageResource(R.drawable.estado3);
        }
        else if (producto.getExistencia() <= 150 && producto.getExistencia() > 100 ){
            holder.txtExistencias.setTextColor(ContextCompat.getColor(context, R.color.colorAdvice));
            holder.imgEstado.setImageResource(R.drawable.estado4);
        }
        else{
            holder.imgEstado.setImageResource(R.drawable.estado5);
            holder.txtExistencias.setTextColor(ContextCompat.getColor(context, R.color.colorLogoV));
        }


        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, DetalleProductoActivity.class);

                intent.putExtra("PRODUCTO", producto);

                context.startActivity(intent);

//                Toast.makeText(context, "click: " + producto.getExistencia(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    /**
     * Devuelve el numero total de elementos del conjunto de datos almacenados en el adaptador.
     * @return Numero total de elementos en este adaptador.
     */
    @Override
    public int getItemCount() {
        return productosList.size();
    }


    /**
     * Clase que implementa lo que se va a mostrar en el reciclerView
     */
    public static class ProductosViewHolder extends RecyclerView.ViewHolder {

        public TextView txtNombre, txtCodigo, txtExistencias;
        public LinearLayout linearLayout;
        private final Context context;


        public ImageView imgEstado;

        public ProductosViewHolder(View itemView) {
            super(itemView);

            context = itemView.getContext();

            txtNombre = (TextView) itemView.findViewById(R.id.TxtNombre);
            txtCodigo = (TextView) itemView.findViewById(R.id.TxtCodigo);
            txtExistencias= (TextView) itemView.findViewById(R.id.TxtExistencias);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.linearLayout_row);
            imgEstado = (ImageView) itemView.findViewById(R.id.ImgEstado);

        }


    }



}
