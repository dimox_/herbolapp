package aplicacion.com.herboapp.util;

import android.support.design.widget.TextInputLayout;

import aplicacion.com.herboapp.R;

/**
 * Clase con metodos estaticos para realizar las validaciones en los formularios de login
 */
public class Validaciones {


    /**
     * Metodo para validar los datos introducidos en un formulario con email y contrasena
     *
     * @param email String con el dato email introducido
     * @param pass  String con el dato password introducido
     * @return true si son validos y false si hay algun error
     */
    public static boolean validarEntradaLogin(TextInputLayout email, TextInputLayout pass) {

        email.setError(null);
        pass.setError(null);

        boolean valid = true;

        String emailStr = email.getEditText().getText().toString();
        String passwordStr = pass.getEditText().getText().toString();

        if (emailStr.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(emailStr).matches()) {
            if (emailStr.isEmpty() || emailStr == null) {
                email.setError(email.getContext().getString(R.string.campo_obligatorio));
                email.requestFocus();
                valid = false;
            } else {
                email.setError(email.getContext().getString(R.string.email_incorrecto));
                email.requestFocus();
                valid = false;
            }

        } else {
            email.setError(null);
        }

        if (passwordStr.isEmpty() || passwordStr.length() < 6 || passwordStr.length() > 10) {
            if (passwordStr.isEmpty() || emailStr == null) {
                pass.setError(pass.getContext().getString(R.string.campo_obligatorio));
//                TilPasswordText.requestFocus();
                valid = false;
            } else {
                pass.setError(pass.getContext().getString(R.string.password_incorrecta));
//                TilPasswordText.requestFocus();
                valid = false;
            }
        } else {
            pass.setError(null);
        }

        return valid;

    }

    /**
     * Metodo para validar un email
     * @param email String con el password
     * @return true si es valido y false si hay algun error
     */
    public static boolean validarEmail(TextInputLayout email) {

        email.setError(null);

        boolean valid = true;

        String emailStr = email.getEditText().getText().toString();

        if (emailStr.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(emailStr).matches()) {
            if (emailStr.isEmpty() || emailStr == null) {
                email.setError(email.getContext().getString(R.string.campo_vacio));
                email.requestFocus();
                valid = false;
            } else {
                email.setError(email.getContext().getString(R.string.email_incorrecto));
                email.requestFocus();
                valid = false;
            }

        } else {
            email.setError(null);
        }

        return valid;

    }

    /**
     * Metodo para validar un password
     * @param pass String con el password
     * @return true si es valido y false si hay algun error
     */
    public static boolean validarPassword(TextInputLayout pass) {

        pass.setError(null);
        pass.setError(null);

        boolean valid = true;

        String passwordStr = pass.getEditText().getText().toString();

        if (passwordStr.isEmpty() || passwordStr.length() < 6 || passwordStr.length() > 10) {
            if (passwordStr.isEmpty() || passwordStr == null) {
                pass.setError(pass.getContext().getString(R.string.campo_vacio));
                valid = false;
            } else {
                pass.setError(pass.getContext().getString(R.string.password_incorrecta));
                valid = false;
            }
        } else {
            pass.setError(null);
        }

        return valid;
    }

    /**
     * Metodo para limpiar el formulario
     */
    public static void limpiarFormulario(TextInputLayout email, TextInputLayout pass) {
        pass.getEditText().setText("");
        email.getEditText().setText("");
        pass.clearFocus();
        email.clearFocus();
    }
}
