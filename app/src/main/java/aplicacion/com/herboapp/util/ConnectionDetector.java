package aplicacion.com.herboapp.util;

import android.app.Service;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Clase para conseguir informacion sobre el estado de la conexion en el dispositivo
 * en un momento solicitado
 */
public class ConnectionDetector {

    // Propiedad para guardar el contexto que vamos a utilizar en la clase
    Context context;

    // Constructor
    public ConnectionDetector(Context context){
        this.context= context;
    }


    /**
     * Metodo que nos devuelve si esta o no conectado
     * @return true si tiene conexion o false en caso contrario
     */
    public boolean isConnected(){

        // Obtenemos la instanmcia del servicio de conexion
        ConnectivityManager connectivity =
                (ConnectivityManager) context
                        .getSystemService(Service.CONNECTIVITY_SERVICE);

        // Realizamos la comprobacion y si es null devuelve false
        if ( connectivity != null ){
            NetworkInfo info = connectivity.getActiveNetworkInfo();
            if ( info != null ){
                if ( info.getState() == NetworkInfo.State.CONNECTED ){
                    return true;
                }
            }

        }
        return false;
    }
}
