package aplicacion.com.herboapp.models;


import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

/**
 * Clase con el modelo producto para almacenar los objetos obtenidos de la base de datos
 * Implementa parcelable para poder pasar los datos entre actividades y Comparable para
 * ordenar los datos en una lista que contenga obhetos de este tipo
 */
public class Producto implements Parcelable,Comparable<Producto> {

    // Atributos
    private String categoria;
    private String codigo;
    private int consumoMaximo;
    private int consumoMinimo;
    private int consumoPromedio;
    private int existencia;
    private String nombre;
    private int tiempoReposicion;

    // Constructores
    public Producto(){}

    public Producto(String categoria, String codigo, int consumoMaximo,
                    int consumoMinimo, int consumoPromedio, int existencia,
                    String nombre, int tiempoReposicion) {
        this.categoria = categoria;
        this.codigo = codigo;
        this.consumoMaximo = consumoMaximo;
        this.consumoMinimo = consumoMinimo;
        this.consumoPromedio = consumoPromedio;
        this.existencia = existencia;
        this.nombre = nombre;
        this.tiempoReposicion = tiempoReposicion;
    }

    public Producto(String nombre, String codigo) {
        this.nombre = nombre;
        this.codigo = codigo;
    }

    protected Producto(Parcel in) {
        categoria = in.readString();
        codigo = in.readString();
        consumoMaximo = in.readInt();
        consumoMinimo = in.readInt();
        consumoPromedio = in.readInt();
        existencia = in.readInt();
        nombre = in.readString();
        tiempoReposicion = in.readInt();
    }

    public static final Creator<Producto> CREATOR = new Creator<Producto>() {
        @Override
        public Producto createFromParcel(Parcel in) {
            return new Producto(in);
        }

        @Override
        public Producto[] newArray(int size) {
            return new Producto[size];
        }
    };

    // Getters y Setters
    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getConsumoMaximo() {
        return consumoMaximo;
    }

    public void setConsumoMaximo(int consumoMaximo) {
        this.consumoMaximo = consumoMaximo;
    }

    public int getConsumoMinimo() {
        return consumoMinimo;
    }

    public void setConsumoMinimo(int consumoMinimo) {
        this.consumoMinimo = consumoMinimo;
    }

    public int getConsumoPromedio() {
        return consumoPromedio;
    }

    public void setConsumoPromedio(int consumoPromedio) {
        this.consumoPromedio = consumoPromedio;
    }

    public int getExistencia() {
        return existencia;
    }

    public void setExistencia(int existencia) {
        this.existencia = existencia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getTiempoReposicion() {
        return tiempoReposicion;
    }

    public void setTiempoReposicion(int tiempoReposicion) {
        this.tiempoReposicion = tiempoReposicion;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(categoria);
        dest.writeString(codigo);
        dest.writeInt(consumoMaximo);
        dest.writeInt(consumoMinimo);
        dest.writeInt(consumoPromedio);
        dest.writeInt(existencia);
        dest.writeString(nombre);
        dest.writeInt(tiempoReposicion);
    }

    public int getExistenciaMinima(){
        return consumoMinimo*tiempoReposicion;
    }

    public int getExistenciaMaxima(){

        return (consumoMaximo*tiempoReposicion)+this.getExistenciaMinima();
    }

    public int getPuntoDePedido (){
        return (consumoPromedio*tiempoReposicion)+this.getExistenciaMinima();
    }

    public int getCantidadDePedido (){
        return this.getExistenciaMaxima()-existencia;
    }

    @Override
    public int compareTo(@NonNull Producto o) {
        if(existencia < o.getExistencia()){
            return -1;
        }
        if(existencia > o.getExistencia()){
            return 1;
        }
        return 0;
    }
}
