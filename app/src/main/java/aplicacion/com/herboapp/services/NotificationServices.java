package aplicacion.com.herboapp.services;


import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import aplicacion.com.herboapp.fireBase.FirebaseReferences;
import aplicacion.com.herboapp.models.Producto;
import aplicacion.com.herboapp.notifications.Notificaciones;

/**
 * Clase que implementa un servicio para mandar notificaciones en segundo plano
 */
public class NotificationServices extends Service{

    private static final int EXISTENCIAS_RIESGO = 150;

    private FirebaseDatabase database;
    private static int cont_prod_riesgo = 0;
    private ValueEventListener ve;
    private static boolean notificacionesActiva = false;

    /**
     * Metodo que se ejecuta cuando se crea la actividad. Obtiene una instancia a Firebase
     */
    @Override
    public void onCreate() {
        database = FirebaseDatabase.getInstance();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Metodo que obtiene una referencia a la base de datos en Firebase y mantiene un observador
     * de los cambio para comunicarlo en tiempo real y activar las notificaciones
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        notificacionesActiva = true;

        ve = database.getReference(FirebaseReferences.PRODUCTOS_REFERENCES)
                .addValueEventListener(new ValueEventListener() {

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        cont_prod_riesgo = 0;
                        for ( DataSnapshot snapshot : dataSnapshot.getChildren() ){
                            Producto producto = snapshot.getValue(Producto.class);

                            if ( producto.getExistencia() <= EXISTENCIAS_RIESGO){
                                cont_prod_riesgo = cont_prod_riesgo + 1;
                            }

                        }

                        if ( cont_prod_riesgo > 0  ){
                            Notificaciones.lanzarNotificacion(getApplicationContext(),
                                    cont_prod_riesgo);
                        }else{
                            Notificaciones.eliminarNotificaciones();
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }

                });

        return START_STICKY;
    }

    /**
     * Metodo que se va ejecutar cuando se cierre la apliacion. Elimina todos los observadores
     * activos a la base de datos Firebase
     */
    @Override
    public void onDestroy() {
        notificacionesActiva = false;
        database.getReference(FirebaseReferences.PRODUCTOS_REFERENCES).removeEventListener(ve);
    }

    /**
     * Metodo para saber si las notificaciones estan activas en la aplicacion
     * @return true si esta activa y false si no lo estan
     */
    public static boolean isNotificacionesActiva(){
        return notificacionesActiva;
    }
}
