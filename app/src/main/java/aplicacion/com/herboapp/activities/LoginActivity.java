package aplicacion.com.herboapp.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import aplicacion.com.herboapp.R;
import aplicacion.com.herboapp.util.ConnectionDetector;
import aplicacion.com.herboapp.util.Validaciones;


/**
 * Clase que implementa la autentificacion de usuarios en una base de datos de Firebase
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private TextInputLayout TilEmailText;
    private TextInputLayout TilPasswordText;

    private EditText edtMail;
    private EditText edtPass;

    private ProgressDialog progressDialog;
    public static FirebaseAuth firebaseAuth = null;
    private Context context = this;
    private static String fechaInicioSesion;

    private static ConnectionDetector cd;

    /**
     * Metodo que se ejecuta cuando se crea la actividad y que hereda de BaseActivity para incluir
     * la toolbar
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        SharedPreferences preferenciasSesion = PreferenceManager.getDefaultSharedPreferences(this);
        fechaInicioSesion = preferenciasSesion.getString("fechaInicio","No establecido");

        // Comprobamos si tiene conexion y en caso contrario avisamos al usuario
        cd = new ConnectionDetector(getApplicationContext());

        // Establecer siempre la orientacion vertical
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        if ( !cd.isConnected() ){
           lanzarAlerta(context,"AVISO: ","Debe estar conectado a Internet para poder" +
                   " usar la aplicación.");
        }

        // Obtenemos la instancia a la BD
         firebaseAuth = FirebaseAuth.getInstance();


        // Si hay un usuario logueado entra directamente en la activity principal de la aplicaion
        if ( firebaseAuth.getCurrentUser() != null ){
            // Perfil de la actividad
            finish();
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }

        // Instancia de la clase ProgressDialog para utilizarla mientras loguea
        progressDialog = new ProgressDialog(this);

        TilEmailText = (TextInputLayout) findViewById(R.id.TilEmail);
        TilPasswordText = (TextInputLayout)findViewById(R.id.TilPass);

        findViewById(R.id.BtnEnviarRegistro).setOnClickListener(this);

        TilEmailText.getEditText().setText("admin@herbolapp.com");
        TilPasswordText.getEditText().setText("123456");

        edtMail = TilEmailText.getEditText();
        edtPass = TilPasswordText.getEditText();

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    /**
     * Metodo que implementa el evento onClick de los botones que esten en modo escucha
     * en la actividad
     * @param v Objeto vista
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.BtnEnviarRegistro:
                if (Validaciones.validarEntradaLogin(TilEmailText, TilPasswordText )) {

                    // Comprobamos si tiene conexion y en caso contrario avisamos al usuario
                    cd = new ConnectionDetector(getApplicationContext());

                    if ( !cd.isConnected() ){
                        lanzarAlerta(context,"AVISO: ","Debe estar conectado a Internet para poder" +
                                " usar la aplicación.");
                    }
                    else {
                    /* Una vez validados los datos del formulario de entrada,
                     se conecta a la BD para comprobar si el usuario es valido
                     y dar acceso a la aplicaion dejando la sesion abierta*/
                        userLogin();
                    }

                }

                break;
        }
    }

    /**
     * Metodo privado para loguearse en Firebase con un email y una contrasena
     */
    private void userLogin(){
        // Utilizamos estas variables temporales para obtener los datos del formulario
        // ya validados previamente
        String email = TilEmailText.getEditText().getText().toString().trim();
        String pass = TilPasswordText.getEditText().getText().toString().trim();

        // Mostramos un mensaje de progreso mientras realiza el login
        progressDialog.setMessage(getResources().getString(R.string.progressLog));
        progressDialog.show();

        // Intenta loguearse con los datos de email y password introducidos
        // con aviso si se ha producido correctamente o ha ocurrodo algun error
        firebaseAuth.signInWithEmailAndPassword(email,pass)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        progressDialog.dismiss();

                        if ( task.isSuccessful() ){
                            // Establecer la fecha de inicio de sesion
                            SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy k:mm");
                            fechaInicioSesion = format1.format(Calendar.getInstance()
                                    .getTime());

                            // A modo de pruba nnos lleva a la actividad para mostrar
                            // los datos de perfil y poder hacer logout
                            finish();
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));

                        }else{
                            lanzarAlerta(context," AVISO: "," Usuario no encontrado.");
//                            Toast.makeText(LoginActivity.this, "Error en Logging", Toast.LENGTH_LONG).show();
                        }

                    }
                });

    }

    public static void lanzarAlerta(Context context, String titulo, String mensage){

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(titulo);
        builder.setMessage(mensage);
        builder.setIcon(R.mipmap.ic_info_alert);
        builder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    public static String getFechaInicioSesion(){
        return fechaInicioSesion;
    }

    public static void setFechaInicioSesion(String fechaInicio){
        fechaInicioSesion = fechaInicio;
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences preferenciasSesion = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor miEditor = preferenciasSesion.edit();
        miEditor.putString("fechaInicio",""+fechaInicioSesion);
        miEditor.apply();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences preferenciasSesion = PreferenceManager.getDefaultSharedPreferences(this);
        fechaInicioSesion = preferenciasSesion.getString("fechaInicio","No establecido");
    }
}