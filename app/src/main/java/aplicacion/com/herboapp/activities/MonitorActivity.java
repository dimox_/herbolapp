package aplicacion.com.herboapp.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

import aplicacion.com.herboapp.R;
import aplicacion.com.herboapp.adapters.ReciclerViewAdapter;
import aplicacion.com.herboapp.fireBase.FirebaseReferences;
import aplicacion.com.herboapp.models.Producto;

/**
 * Clase con la actividad que implementa un reciclerView para mostrar todos los datos de
 * productos obtenidos de la base de datos Firebase
 */
public class MonitorActivity extends BaseActivity {

    private static final int EXISTENCIAS_RIESGO = 150;

    private RecyclerView reciclerView;
    private static ArrayList<Producto> productosList;
    private ReciclerViewAdapter adapter;
    private FirebaseDatabase database;
    private static int cont_prod_riesgo = 0;

    /**
     * Metodo que se ejecuta cuando se crea la actividad y que hereda de BaseActivity
     * para implememtar la toolbar
     */
    @SuppressLint("MissingSuperCall")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_monitor);

        // Titulo de la actividad en la toolbar
        getSupportActionBar().setTitle("Estado del stock");


        reciclerView = (RecyclerView) findViewById(R.id.RecyclerMonitor);

        reciclerView.setLayoutManager(new LinearLayoutManager(this));

        productosList = new ArrayList<>();

        database = FirebaseDatabase.getInstance();

        adapter = new ReciclerViewAdapter(productosList);

        reciclerView.setAdapter(adapter);


        firebaseObservable();

    }

    /**
     * Metodo para conectarse a una referencia de base de datos en Firebase e inicializar un
     * ArrayList con todos los datos obtenidos, manteniendo un observador de los cambios
     * producidos y reflejarlos en la lista en tiempo real
     */
    public void firebaseObservable(){

        database.getReference(FirebaseReferences.PRODUCTOS_REFERENCES).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                cont_prod_riesgo = 0;
                reciclerView.scrollToPosition(0);  // Volver al principio del reciclerview
                productosList.removeAll(productosList); // Borrar todos los elementos de la lista

                for ( DataSnapshot snapshot : dataSnapshot.getChildren() ){
                    Producto producto = snapshot.getValue(Producto.class);
                    if ( producto.getExistencia() <= EXISTENCIAS_RIESGO){
                        cont_prod_riesgo = cont_prod_riesgo + 1;
                    }
                    productosList.add(producto);

                }

                Collections.sort(productosList); // Ordenar existencias ASC
                adapter.notifyDataSetChanged();  // Notificar al adaptador

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
//                Toast.makeText(MonitorActivity.this,
//                        "ERROR: " + databaseError.getCode(), Toast.LENGTH_SHORT).show();
            }

        });

    }

}
