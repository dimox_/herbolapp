package aplicacion.com.herboapp.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import aplicacion.com.herboapp.R;
import aplicacion.com.herboapp.util.Validaciones;

/**
 * Clase con la actividad que implementa la funcionalidad para registrar un usuario en Firebase
 */
public class RegistroUsuariosActivity extends BaseActivity implements View.OnClickListener{

    private TextInputLayout TilEmailText;
    private TextInputLayout TilPasswordText;

    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;

    /**
     * Metodo que se ejecuta cuando se crea la actividad y que hereda de BaseActivity
     * para implememtar la toolbar
     */
    @SuppressLint("MissingSuperCall")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState,R.layout.activity_registro_usuarios);

        // Titulo de la actividad en la toolbar
        getSupportActionBar().setTitle("Añadir usuarios");

        // Con esto activamos  o desactivamos la flecha en la toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Establecer siempre la orientacion vertical
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Obtenemos la instancia a la BD
        firebaseAuth = LoginActivity.firebaseAuth;

        // Instancia de la clase ProgressDialog para utilizarla mientras registra el usuario
        progressDialog = new ProgressDialog(this);

        TilEmailText = (TextInputLayout) findViewById(R.id.TilEmail);
        TilPasswordText = (TextInputLayout)findViewById(R.id.TilPass);

        findViewById(R.id.BtnEnviarRegistro).setOnClickListener(this);

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
    }

    /**
     * Metodo que implementa el evento onClick para todos los componentes de la actividad que
     * se encuentren en modo escucha
     * @param v Objeto de tipo View
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.BtnEnviarRegistro:
                    if (Validaciones.validarEntradaLogin(TilEmailText, TilPasswordText) ) {
                        // Accedemos al metodo para registrar usuario en la BD
                        registroUsuario();
                    }

            break;

        }
    }

    /**
     * Metodo para realizar el registro de un usuario en Firebase con un email y una contrasena
     */
    public void registroUsuario(){

        // Utilizamos estas variables temporales para obtener los datos del formulario
        // ya validados previamente
        String email = TilEmailText.getEditText().getText().toString().trim();
        String pass = TilPasswordText.getEditText().getText().toString().trim();


        progressDialog.setMessage(getResources().getString(R.string.progressReg));
        progressDialog.show();



        // Creamos un nuevo usuario con los datos introducidos
        firebaseAuth.createUserWithEmailAndPassword(email,pass).
                addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        // Cerramos el processDialog
                        progressDialog.dismiss();

                        if ( task.isSuccessful() ){

                            // Establecer la fecha de inicio de sesion
                            SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy k:mm");
                            LoginActivity.setFechaInicioSesion(format1.format(Calendar
                                    .getInstance().getTime()));

                            // Usuario registrado y logueado
                            Toast toast = Toast.makeText(RegistroUsuariosActivity.this, getResources()
                                            .getString(R.string.registro_ok),
                                    Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER,10,10);
                            toast.show();

                        }else{
                            // Usuario no registrado
                            Toast toast = Toast.makeText(RegistroUsuariosActivity.this,
                                    getResources()
                                            .getString(R.string.registro_error),
                                    Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER,10,10);
                            toast.show();
                        }

                        // Limpiamos el formulario para registar nuevos usuarios
                        Validaciones.limpiarFormulario(TilEmailText, TilPasswordText);
                    }
                });

    }

}

