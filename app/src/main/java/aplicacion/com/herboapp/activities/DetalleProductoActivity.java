package aplicacion.com.herboapp.activities;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

import aplicacion.com.herboapp.R;
import aplicacion.com.herboapp.models.Producto;


/**
 * Clase para mostrar los detalles de un producto. Está vinculado a una vista donde se
 * mostrara la siguiente informacion: una gráfica con la representación lineal de la
 * existencia máxima, mínima y punto de pedido. Nombre, Código y existencias del producto.
 * Existencia máxima, existencia mínima, punto de pedido, cantidad de pedido, tiempo de
 * reposición, consumo promedio, consumo máximo, consumo mínimo.
 */
public class DetalleProductoActivity extends BaseActivity {

    private TextView txtExisMax, txtExisMin, txtPuntoPed, txtCantPed,
            edtTr, edtCp, edtCM, edtCm, txtNombreProducto, txtCodigo, txtExistencia;


    /**
     * Metodo que se ejecuta cuando se crea la actividad y que hereda de BaseActivity
     * para implememtar la toolbar
     */
    @SuppressLint("MissingSuperCall")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_detalle_producto);

        // Titulo de la actividad en la toolbar
        getSupportActionBar().setTitle("Detalle del producto");


        // Establecer siempre la orientacion vertical
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Obtenemos los datos del item pulsado en el reciclerview de la actividad monitor
        Bundle extras = getIntent().getExtras();

        Producto producto = extras.getParcelable("PRODUCTO");

        // Variables auxiliares para cargar los datos obtenidos
        int Em, EM, Pp;
        Em = producto.getExistenciaMinima();
        EM = producto.getExistenciaMaxima();
        Pp = producto.getPuntoDePedido();


        // Creamos un objeto LineChart vinculado a su layout
        LineChart lineChart = (LineChart) findViewById(R.id.chart);

        // Introducimos los datos en un arraylist
        ArrayList<Entry> entries = new ArrayList<>();
        entries.add(new Entry((float)EM, 0));
        entries.add(new Entry((float)Pp, 1));
        entries.add(new Entry((float)Em, 2));

        // Cargamos el dataset con los datos
        LineDataSet dataset = new LineDataSet(entries, "NIVEL DE EXISTENCIAS");

        // Creamos otro arraylist con los labels del grafico
        ArrayList<String> labels = new ArrayList<String>();
        labels.add("EM");
        labels.add("Pp");
        labels.add("Em");

        // Se cargan los datos y se construye el grafico con las caracteristicas especificadas
        LineData data = new LineData(labels, dataset);
        dataset.setColors(ColorTemplate.COLORFUL_COLORS); //
        dataset.setDrawCubic(false);
        dataset.setDrawFilled(true);

        lineChart.setData(data);
        lineChart.animateY(2000);

        // Activar esta liunea para introducir un titulo a la grafiuca
         lineChart.setDescription("");


        txtNombreProducto = (TextView) findViewById(R.id.TxtNombreProducto);
        txtCodigo = (TextView) findViewById(R.id.TxtCodigo);
        txtExistencia = (TextView) findViewById(R.id.TxtExistencia);

        txtExisMax = (TextView) findViewById(R.id.TxtExisMax);
        txtExisMin = (TextView) findViewById(R.id.TxtExisMin);
        txtPuntoPed = (TextView) findViewById(R.id.TxtPuntoPed);
        txtCantPed= (TextView) findViewById(R.id.TxtCantPed);

        edtTr= (TextView) findViewById(R.id.EdtTr);
        edtCp = (TextView) findViewById(R.id.EdtCp);
        edtCM = (TextView) findViewById(R.id.EdtCM);
        edtCm= (TextView) findViewById(R.id.EdtCm);

        txtExisMax.setText(String.valueOf(producto.getExistenciaMaxima()));
        txtExisMin.setText(String.valueOf(producto.getExistenciaMinima()));
        txtPuntoPed.setText(String.valueOf(producto.getPuntoDePedido()));
        txtCantPed.setText(String.valueOf(producto.getCantidadDePedido()));

        txtNombreProducto.setText(String.valueOf(producto.getNombre()));
        txtCodigo.setText(String.valueOf(producto.getCodigo()));
        txtExistencia.setText(String.valueOf(producto.getExistencia()));

        edtTr.setText(String.valueOf(producto.getTiempoReposicion()));
        edtCp.setText(String.valueOf(producto.getConsumoPromedio()));
        edtCM.setText(String.valueOf(producto.getConsumoMaximo()));
        edtCm.setText(String.valueOf(producto.getConsumoMinimo()));

    }


}
