package aplicacion.com.herboapp.activities;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import aplicacion.com.herboapp.R;
import aplicacion.com.herboapp.fireBase.FirebaseReferences;
import aplicacion.com.herboapp.models.Producto;
import aplicacion.com.herboapp.services.NotificationServices;

/**
 * Clase con la actividad que implementa la funcionalidad del menu principal de la aplicacion
 */
public class MainActivity extends BaseActivity implements View.OnClickListener {

    private Button btnNotificaciones = null;
    private static ArrayList<Producto> productosList=null;
    private FirebaseDatabase database;
    private static boolean modoAdmin = false;

    /**
     * Metodo que se ejecuta cuando se crea la actividad y que hereda de BaseActivity
     * para implememtar la toolbar
     */
    @SuppressLint("MissingSuperCall")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState,R.layout.activity_main);

        // Titulo de la actividad en la toolbar
        getSupportActionBar().setTitle("Menú pricipal");

        // Con esto activamos  o desactivamos la flecha en la toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        // Establecer siempre la orientacion vertical
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

       (findViewById(R.id.btnBuscar)).setOnClickListener(this);
       (findViewById(R.id.btnAnadirUsuario)).setOnClickListener(this);
       (findViewById(R.id.btnMonitor)).setOnClickListener(this);

        btnNotificaciones = (Button) findViewById(R.id.btnAlarma);
        btnNotificaciones.setOnClickListener(this);

        if (NotificationServices.isNotificacionesActiva()){
            btnNotificaciones.setText("Notificaciones ON");
            btnNotificaciones.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            btnNotificaciones.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_alarm_on, 0, 0);
        }else{
            btnNotificaciones.setText("Notificaciones OFF");
            btnNotificaciones.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBotonPulsado));
            btnNotificaciones.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_alarm_off, 0, 0);
        }

        String email = FirebaseAuth.getInstance().getCurrentUser().getEmail();
        if (email.equalsIgnoreCase("admin@herbolapp.com")){
            modoAdmin = true;
        }else{
            modoAdmin = false;
        }

        database = FirebaseDatabase.getInstance();

        productosList = new ArrayList<>();

        firebaseObservable();


    }

    /**
     * Metodo que implementa el evento onClick para todos los componentes de la actividad que
     * se encuentren en modo escucha
     * @param v Objeto de tipo View
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnBuscar:
                startActivity(new Intent(this, LectorCodeBarActivity.class));
                break;

            case R.id.btnMonitor:
                startActivity(new Intent(this, MonitorActivity.class));
                break;

            case R.id.btnAlarma:
                cambiarEstadoNotificaciones();
                break;

            case R.id.btnAnadirUsuario:

                if (modoAdmin) {
                    startActivity(new Intent(this, RegistroUsuariosActivity.class));
                }
                else{
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle(" AVISO: ");
                    builder.setMessage("Para crear usuarios nuevos " +
                            "tiene que iniciar sesión como administrador");
                    builder.setIcon(R.mipmap.ic_info_alert);
                    builder.setCancelable(false)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
                break;

            default:
                break;

        }
    }

    /**
     * Metodo que se ejcuta cuando la aplicacion viene de otro estado y se vuelve a ejecutar.
     * Obtiene una instancia del usuario que hay activo y si coincide con la cuenta de
     * administrador cambia el valor de la variable estatica modoAdmin a true o false si no
     * coincide
     */
    @Override
    protected void onRestart() {
        super.onRestart();
        String email = FirebaseAuth.getInstance().getCurrentUser().getEmail();
        if (email.equalsIgnoreCase("admin@herbolapp.com")){
            modoAdmin = true;
        }else{
            modoAdmin = false;
        }
    }

    /**
     * Metodo para conectarse a una referencia de base de datos en Firebase e inicializar un
     * ArrayList con todos los datos obtenidos, manteniendo un observador de los cambios
     * producidos y reflejarlos en la lista en tiempo real
     */
    public void firebaseObservable(){

        database.getReference(FirebaseReferences.PRODUCTOS_REFERENCES)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        productosList.removeAll(productosList); // Borrar todos los elementos de la lista
                        for ( DataSnapshot snapshot : dataSnapshot.getChildren() ){
                            Producto producto = snapshot.getValue(Producto.class);
                            productosList.add(producto);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }


    /**
     * Metodo para activar o desactivar las notificaciones cambiando su estado al contrario del
     * estado que este en ese momento
     */
    private void cambiarEstadoNotificaciones()
    {
        if (!NotificationServices.isNotificacionesActiva())
        {
            // Si las notificaciones estan desactivadas, las activa
            activarNotificaciones();
        }else
        {
            // Si las notificaciones estan activadas, las desactiva
            desactivarNotificaciones();
        }
    }

    /**
     * Metodo que desactiva las notificaciones
     */
    private void desactivarNotificaciones()
    {
        stopService(new Intent(this, NotificationServices.class));
        btnNotificaciones.setText("Notificaciones OFF");
        btnNotificaciones.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBotonPulsado));
        btnNotificaciones.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_alarm_off, 0, 0);

//        Toast.makeText(MainActivity.this, "Alarma detenida", Toast.LENGTH_LONG).show();

    }

    /**
     * Metodo que activa las notificaciones
     */
    private void activarNotificaciones()
    {

        startService(new Intent(this,NotificationServices.class));
        btnNotificaciones.setText("Notificaciones ON");
        btnNotificaciones.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        btnNotificaciones.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_alarm_on, 0, 0);

//        Toast.makeText(MainActivity.this, "Alarma iniciada", Toast.LENGTH_LONG).show();

    }

    /**
     * Metodo para saber si se esta en modo administrador
     * @return true si esta en modo administrador y false si esta en modo usuario
     */
    public static boolean isModoAdmin(){
        return modoAdmin;
    }

    /**
     * Metodo estatico para obtener un producto
     * @param codigo String con el codigo del producto
     * @return el objeto producto que su atributo codigo sea igual al introducido
     * como paramentro o null si no lo encuentra
     */
    @Nullable
    public static Producto getProductoPorCodigo(String codigo){

        for (Producto pro : productosList ) {
            if (pro.getCodigo().equalsIgnoreCase(codigo)) {
                return pro;
            }
        }
        return  null;
    }


}
