package aplicacion.com.herboapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import aplicacion.com.herboapp.R;

/**
 * Clase que implementa la funcionalidad de la toolbar para ser heredada por otras clases que
 * quieran anadirla
 */
public class BaseActivity extends AppCompatActivity{

    private FirebaseAuth firebaseAuth;

    /**
     * Metodo que se ejecuta cuando se crea la actividad.
     * @param layoutID Layout en el que se va a aplicar la funcionalizad implementada en esta clase
     */
    protected final void onCreate(Bundle savedInstanceState, int layoutID) {
        super.onCreate(savedInstanceState);
        setContentView(layoutID);

        if ( firebaseAuth == null) {
            firebaseAuth = FirebaseAuth.getInstance();
        }

        // Implementacion de la Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Activamos  o desactivamos la flecha en la toolbar
//         getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                  finish();
            }
        });


    }

    /**
     * Metodo para crear el menu de la toolbar
     * @param menu Archivo xml con el diseno del menu
     * @return El menu creado
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    /**
     * Metodo para determinar la accion a tomar al hacer click en los items del menu
     * @param item El item seleccionado
     * @return El item por defecto
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.accInicio:
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                return true;
            case R.id.accUsuarios:
                startActivity(new Intent(getApplicationContext(), PerfilActivity.class));
                return true;
            case R.id.accBuscarProducto:
                finish();
                startActivity(new Intent(getApplicationContext(), LectorCodeBarActivity.class));
                return true;
            case R.id.accCerrar:
                if ( firebaseAuth != null ) {
                    firebaseAuth.signOut();
                }
                finish();
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Metoto para mostar un mensaje con un texto desde un recurso
     * @param resid recurso de string a utilizar
     */
    private void action(int resid) {
        Toast.makeText(this, getText(resid), Toast.LENGTH_SHORT).show();
    }

}
