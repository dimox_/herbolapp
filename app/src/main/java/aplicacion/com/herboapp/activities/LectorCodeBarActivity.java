package aplicacion.com.herboapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import java.util.ArrayList;

import aplicacion.com.herboapp.models.Producto;
import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;


/**
 * Clase que implementa la libreria ZBarScannerView para crear un lector de codigo de barras
 */
public class LectorCodeBarActivity extends Activity implements ZBarScannerView.ResultHandler {

    private ZBarScannerView mScannerView;

    private String resultado;
    private ArrayList<Producto> listProductos = null;
    private Producto producto = null;

    /**
     * Metodo que se ejecuta cuando se crea la actividad. Inicializa el scanner y establece
     * la vista
     */
    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        mScannerView = new ZBarScannerView(this);    // Inicializa el scanner
        setContentView(mScannerView);                // establece el contenido de la vista
    }

    /**
     * Metodo que registra los resultado del scanner y inicia la camara al comenzar
     */
    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Registra los resultado del scanner.
        mScannerView.startCamera();          // inicia la camara
    }

    /**
     * Metodo que para la camara cuando la actividad esta en pausa
     */
    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Para la camara en pausa
    }

    /**
     * Metodo que implementa lo que se hace con el resultado obtenido del scanner y
     * realiza las operaciones indicadas.
     *
     * @param rawResult resultado del scanner
     */
    @Override
    public void handleResult(Result rawResult) {

        boolean encontrado = false;
        resultado = rawResult.getContents();


        producto = MainActivity.getProductoPorCodigo(resultado);


        if (producto != null) {
            encontrado = true;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Resultado del scanner: ");
        if (encontrado) {
            builder.setMessage(producto.getNombre() + "\nCódigo: " + resultado);
        } else {
            builder.setMessage("PRODUCTO NO ENCONTRADO\nCódigo: " + resultado);
        }
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        // If you would like to resume scanning, call this method below:
//        mScannerView.resumeCameraPreview(this);


        if (encontrado) {

            mScannerView.stopCameraPreview();
            Intent intent = new Intent(this, DetalleProductoActivity.class);

            intent.putExtra("PRODUCTO", producto);
            startActivity(intent);

            finish();
        } else {
            mScannerView.resumeCameraPreview(this);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        startActivity(new Intent(this, MainActivity.class));
    }
}
