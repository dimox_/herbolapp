package aplicacion.com.herboapp.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import aplicacion.com.herboapp.R;

/**
 * Clase con la actividad que implementa la funcionalidad para mostrar
 * los datos del usuario activo
 */
public class PerfilActivity extends BaseActivity implements View.OnClickListener {

    private FirebaseAuth firebaseAuth;
    private TextView txtUsuario;
    private TextView txtPrivilegios;
    private TextView txtFechaLog;
    private ImageView imgPerfil;

    /**
     * Metodo que se ejecuta cuando se crea la actividad y que hereda de BaseActivity
     * para implememtar la toolbar
     */
    @SuppressLint("MissingSuperCall")
    protected final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState,R.layout.activity_perfil);

        // Titulo de la actividad en la toolbar
        getSupportActionBar().setTitle("Usuario actual");

        // Establecer siempre la orientacion vertical
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        firebaseAuth = FirebaseAuth.getInstance();

        if ( firebaseAuth.getCurrentUser() == null ){
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        FirebaseUser usuario = firebaseAuth.getCurrentUser();

        imgPerfil = (ImageView) findViewById(R.id.ImgPerfil);

        txtUsuario = (TextView) findViewById(R.id.TxtUsuario);
        txtUsuario.setText(usuario.getEmail());

        txtPrivilegios = (TextView) findViewById(R.id.TxtPrivilegios);
        if ( FirebaseAuth.getInstance().getCurrentUser()
                .getEmail().equalsIgnoreCase("admin@herbolapp.com") ){
            txtPrivilegios.setText("ADMINISTRADOR");
            imgPerfil.requestLayout();
            imgPerfil.getLayoutParams().height = 400;
            imgPerfil.setImageDrawable(establecerImagenRedondeada(ContextCompat
                    .getDrawable(PerfilActivity.this, R.drawable.imgadminuser)));
        }else{
            txtPrivilegios.setText("USUARIO");
        }

        txtFechaLog = (TextView) findViewById(R.id.TxtInicioLog);
        txtFechaLog.setText(LoginActivity.getFechaInicioSesion());

        findViewById(R.id.BtnLogout).setOnClickListener(this);


    }

    /**
     * Metodo que implementa el evento onClick para todos los componentes de la actividad que
     * se encuentren en modo escucha
     * @param v Objeto de tipo View
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.BtnLogout:
                firebaseAuth.signOut();
                finish();
                startActivity(new Intent(this, LoginActivity.class));
                break;
        }
    }

    public RoundedBitmapDrawable establecerImagenRedondeada(Drawable originalDrawable){

        //extraemos el drawable en un bitmap
        Bitmap originalBitmap = ((BitmapDrawable) originalDrawable).getBitmap();

        //creamos el drawable redondeado
        RoundedBitmapDrawable roundedDrawable =
                RoundedBitmapDrawableFactory.create(getResources(), originalBitmap);

        //asignamos el CornerRadius
        roundedDrawable.setCornerRadius(originalBitmap.getHeight());

        // Devolvemos la imagen redondeada
        return roundedDrawable;

    }


    // Habilitar en caso de querer añadir un boton para eliminar
    // el usuario que esta en ese momento logueado, ya que todavia
    // no existe una manera de hacerlo en firebase sin estar logueado
   /* public void eliminarUsuario(){

        FirebaseUser usuario = FirebaseAuth.getInstance().getCurrentUser();

        usuario.delete()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(RegistroUsuariosActivity.this, "Eliminado",
                                    Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(RegistroUsuariosActivity.this, "No se ha podido eliminar",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }*/
}
