package aplicacion.com.herboapp.notifications;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;

import aplicacion.com.herboapp.R;
import aplicacion.com.herboapp.activities.MonitorActivity;

/**
 * Clase con metodos estaticos para crear notificaciones
 */
public class Notificaciones {

    public static final int ID_NOTIFICACION = 1;
    private static NotificationManager nManager;
    private static NotificationCompat.Builder builder;

    /**
     * Metodo para lanzar un notificacion
     * @param context El contexto actual
     * @param num_prod_riesgo Dato sobre el numero de productos en riesgo en la base de datos para
     *                        mostrarlo en la notificacion
     */
    public static void lanzarNotificacion(Context context, int num_prod_riesgo){

        Intent intent = new Intent(context, MonitorActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context,0,intent,0);

        Bitmap icono_nitificacion = BitmapFactory.decodeResource(context.getResources(),R.drawable.ic_notification_large);

        builder = new NotificationCompat.Builder(context);
        builder.setSmallIcon(android.R.drawable.ic_dialog_alert)
                .setLargeIcon(icono_nitificacion)
                .setContentTitle(num_prod_riesgo + " Producto/s bajo mínimos")               // Titulo de la notificacion
                .setContentText("Pulse para ver cuales son...")             // Contenido de la notificacion
//                .setContentInfo("info")                  // Texto en la parte inferior derecha
                .setTicker("Tiene avisos de Herbolapp")  // Texto que aparece en la notificacion
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)                     // Se cierra cuando se produce el evento
                .setOngoing(true)                        // Obliga a entrar para cerrar
                .setPriority(Notification.PRIORITY_MAX)  // Prioridad maxima a la notificacion
                .setVibrate(new long[] {500,1000}) ;     // Patron de vibracion


        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(uri);   // Establece el sonido de notificaciones por defecto del systema

        nManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        nManager.notify(ID_NOTIFICACION,builder.build());


    }

    /**
     * Metodo para eliminar una notificacion
     */
    public static void eliminarNotificaciones(){
        if (nManager != null)
        nManager.cancel(ID_NOTIFICACION);
    }
}
